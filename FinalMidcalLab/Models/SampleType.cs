﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalMidcalLab.Models
{
    public class SampleType
    {
        public SampleType()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Sample> Samples { get; set; }
    }
}
