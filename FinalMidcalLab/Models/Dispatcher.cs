﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalMidcalLab.Models
{
    public class Dispatcher
    {
        public Dispatcher()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public ICollection<Patient> Patients { get; set; }
    }
}
