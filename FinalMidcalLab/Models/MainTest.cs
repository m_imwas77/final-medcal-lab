﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalMidcalLab.Models
{
    public class MainTest
    {
        public MainTest()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Test>  Tests { get; set; }
    }
}
