﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalMidcalLab.Models
{
    public class Result
    {
        public Result()
        {

        }
        public int Id { get; set; } 
        public int ResultValue { get; set; }

        //public int SampleId { get; set; }
        //public Sample Sample { get; set; }


        public int PatientId { get; set; }
        public Patient Patient { get; set; }


        public int TestId { get; set; }
        public Test Test{ get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee{ get; set; }

    }
}
