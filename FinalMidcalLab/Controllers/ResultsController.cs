﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalMidcalLab.Data;
using FinalMidcalLab.Models;
using Microsoft.AspNetCore.Authorization;

namespace FinalMidcalLab.Controllers
{
    [Authorize]
    public class ResultsController : Controller
    {
        private readonly BackEndDbContext _context;

        public ResultsController(BackEndDbContext context)
        {
            _context = context;
        }

        // GET: Results
        public async Task<IActionResult> Index()
        {
            var backEndDbContext = _context.Results.Include(r => r.Employee).Include(r => r.Patient).Include(r => r.Test);
            return View(await backEndDbContext.ToListAsync());
        }

        // GET: Results/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var result = await _context.Results
                .Include(r => r.Employee)
                .Include(r => r.Patient)
                .Include(r => r.Test)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (result == null)
            {
                return NotFound();
            }

            return View(result);
        }

        // GET: Results/Create
        public IActionResult Create()
        {
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "Id", "Name");
            ViewData["PatientId"] = new SelectList(_context.Patients, "Id", "FirstName");
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Name");
            return View();
        }

        // POST: Results/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ResultValue,PatientId,TestId,EmployeeId")] Result result)
        {
            if (ModelState.IsValid)
            {
                _context.Add(result);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "Id", "Id", result.EmployeeId);
            ViewData["PatientId"] = new SelectList(_context.Patients, "Id", "Id", result.PatientId);
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Id", result.TestId);
            return View(result);
        }

        // GET: Results/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var result = await _context.Results.FindAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "Id", "Name", result.EmployeeId);
            ViewData["PatientId"] = new SelectList(_context.Patients, "Id", "FirstName", result.PatientId);
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Name", result.TestId);
            return View(result);
        }

        // POST: Results/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ResultValue,PatientId,TestId,EmployeeId")] Result result)
        {
            if (id != result.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(result);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ResultExists(result.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "Id", "Id", result.EmployeeId);
            ViewData["PatientId"] = new SelectList(_context.Patients, "Id", "Id", result.PatientId);
            ViewData["TestId"] = new SelectList(_context.Tests, "Id", "Id", result.TestId);
            return View(result);
        }

        // GET: Results/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var result = await _context.Results
                .Include(r => r.Employee)
                .Include(r => r.Patient)
                .Include(r => r.Test)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (result == null)
            {
                return NotFound();
            }

            return View(result);
        }

        // POST: Results/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await _context.Results.FindAsync(id);
            _context.Results.Remove(result);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public IActionResult Report(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var result = _context.Results.Where(c => c.Id == id).Select(t=>new
            {
                EmployeeName = t.Employee.Name,
                EmployeeAddress = t.Employee.Address,
                EmployeeGender = t.Employee.Gender.ToString(),
                EmployeePhone = t.Employee.Phone,
                EmployeeEmail = t.Employee.Email,
                TestName = t.Test.Name,
                TestValue = t.Test.Value,
                PatiantFirstName = t.Patient.FirstName,
                PatiantLastName = t.Patient.LastName,
                PatiantAge = t.Patient.Age,
                PataintGender = t.Patient.Gender.ToString(),
                ResultValue = t.ResultValue
            });
            if (result == null)
            {
                return NotFound();
            }
            ViewData["result"] = Json(result);
            //return Json(result);
            return View();

        }
        private bool ResultExists(int id)
        {
            return _context.Results.Any(e => e.Id == id);
        }
    }
}

//@{
//    ViewData["Title"] = "Report";
//    var inc = Newtonsoft.Json.JsonConvert.SerializeObject(ViewBag.result);
//JObject joResponse = JObject.Parse(inc);
//JArray value = (JArray)joResponse["Value"];
//var s = value[0];
//}

//<h2>@s["EmployeeAddress"]</h2>
//<h2>@s["EmployeeName"]</h2>
//<h2>@s["EmployeeGender"]</h2>
//<h2>@s["ResultValue"]</h2>

