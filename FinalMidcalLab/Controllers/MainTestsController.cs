﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalMidcalLab.Data;
using FinalMidcalLab.Models;
using Microsoft.AspNetCore.Authorization;

namespace FinalMidcalLab.Controllers
{
    [Authorize]
    public class MainTestsController : Controller
    {
        private readonly BackEndDbContext _context;

        public MainTestsController(BackEndDbContext context)
        {
            _context = context;
        }

        // GET: MainTests
        public async Task<IActionResult> Index()
        {

            return View(await _context.MainTests.ToListAsync());
        }

        // GET: MainTests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainTest = await _context.MainTests
                .FirstOrDefaultAsync(m => m.Id == id);
            if (mainTest == null)
            {
                return NotFound();
            }

            return View(mainTest);
        }

        // GET: MainTests/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MainTests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] MainTest mainTest)
        {
            if (ModelState.IsValid)
            {
                _context.Add(mainTest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(mainTest);
        }

        // GET: MainTests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainTest = await _context.MainTests.FindAsync(id);
            if (mainTest == null)
            {
                return NotFound();
            }
            return View(mainTest);
        }

        // POST: MainTests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] MainTest mainTest)
        {
            if (id != mainTest.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(mainTest);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MainTestExists(mainTest.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(mainTest);
        }

        // GET: MainTests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainTest = await _context.MainTests
                .FirstOrDefaultAsync(m => m.Id == id);
            if (mainTest == null)
            {
                return NotFound();
            }

            return View(mainTest);
        }

        // POST: MainTests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mainTest = await _context.MainTests.FindAsync(id);
            _context.MainTests.Remove(mainTest);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MainTestExists(int id)
        {
            return _context.MainTests.Any(e => e.Id == id);
        }
    }
}
