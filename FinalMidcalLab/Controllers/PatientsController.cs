﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FinalMidcalLab.Data;
using FinalMidcalLab.Models;
using Microsoft.AspNetCore.Authorization;

namespace FinalMidcalLab.Controllers
{
    [Authorize]
    public class PatientsController : Controller
    {
       
        private readonly BackEndDbContext _context;

        public PatientsController(BackEndDbContext context)
        {
            _context = context;
        }

        // GET: Patients
        public async Task<IActionResult> Index()
        {
            var backEndDbContext = _context.Patients.Include(p => p.Dispatcher);
            return View(await backEndDbContext.ToListAsync());
        }

        // GET: Patients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .Include(p => p.Dispatcher)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // GET: Patients/Create
        public IActionResult Create()
        {
            ViewData["DispatcherId"] = new SelectList(_context.Dispatchers, "Id", "Name");
            var gender = new[]
            {
                new {id = 0 , name = "male"},
                new { id = 1 , name = "female"}
            }.ToList();
            ViewData["gender"] = new SelectList(gender, "id", "name");
            return View();
        }

        // POST: Patients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Gender,Age,DispatcherId")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patient);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DispatcherId"] = new SelectList(_context.Dispatchers, "Id", "Id", patient.DispatcherId);
            return View(patient);
        }

        // GET: Patients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients.FindAsync(id);
            if (patient == null)
            {
                return NotFound();
            }
            var gender = new[]
            {
                new {id = 0 , name = "male"},
                new { id = 1 , name = "female"}
            }.ToList();
            ViewData["gender"] = new SelectList(gender, "id", "name");
            ViewData["DispatcherId"] = new SelectList(_context.Dispatchers, "Id", "Name", patient.DispatcherId);
            return View(patient);
        }

        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Gender,Age,DispatcherId")] Patient patient)
        {
            if (id != patient.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DispatcherId"] = new SelectList(_context.Dispatchers, "Id", "Id", patient.DispatcherId);
            return View(patient);
        }

        // GET: Patients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .Include(p => p.Dispatcher)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patient = await _context.Patients.FindAsync(id);
            _context.Patients.Remove(patient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        // Get : Patients/ReportPationt/2
        [HttpGet]
        public  IActionResult ReportPationt(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var Result = _context.Results.Include(t => t.Patient).Where(c => c.PatientId == id)
                .Select(t => new {
                    PatientFirstName = t.Patient.FirstName,
                    PatientLastName = t.Patient.LastName,
                    PatientGender = t.Patient.Gender.ToString(),
                    PatientAge = t.Patient.Age,
                    Result = t.ResultValue,
                    TestName = t.Test.Name,
                    TestValue = t.Test.Value
                } );
            if (Result == null)
            {
                return NotFound();
            }
            return Json(Result);
        }

        // Get : Patients/EmploeeyResult/2
        [HttpGet]
        public IActionResult EmploeeyResult(int? id)
        { 
            if (id == null)
            {
                return NotFound();
            }
            var Result = _context.Results.Where(c => c.EmployeeId == id)
                .Select(t => new {
                   EmployeeName = t.Employee.Name,
                   EmployeeAddress = t.Employee.Address,
                   EmployeeGender = t.Employee.Gender.ToString(),
                   EmployeePhone = t.Employee.Phone,
                   EmployeeEmail = t.Employee.Email,
                   TestName = t.Test.Name,
                   TestValue = t.Test.Value,
                   PatiantFirstName = t.Patient.FirstName,
                   PatiantLastName = t.Patient.LastName,
                   PatiantAge = t.Patient.Age,
                   PataintGender = t.Patient.Gender.ToString(),
                   ResultValue = t.ResultValue
                });
            if (Result == null)
            {
                return NotFound();
            }
            ViewData["result"] = Result;
            return View();
        }

        // Get : Patients/DispatcerPataint/2
        [HttpGet]
        public IActionResult DispatcerPataint(int? id)
        { 
            if (id == null)
            {
                return NotFound();
            }
            var Result = _context.Dispatchers.Include(c=>c.Patients).Where(c=>c.Id == id).
                Select(t=>new
                {
                    dispatchersName = t.Name,
                    dispatchersPhone = t.Phone,
                    patients = t.Patients
                });
            if (Result == null)
            {
                return NotFound();
            }
            ViewData["result"] = Result;
            return View();
        }
        // Get : Patients/TestPataint/2
        [HttpGet]
        public IActionResult TestPataint(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var Result = _context.Results.Include(c => c.Test).Where(c => c.TestId == id).Select(t => new
            {
                testName = t.Test.Name,
                testValue = t.Test.Value,
                PatiantFirstName = t.Patient.FirstName,
                PatiantLastName = t.Patient.LastName,
                PatiantAge = t.Patient.Age,
                PataintGender = t.Patient.Gender.ToString(),
                Value = t.ResultValue

            });
            if (Result == null)
            {
                return NotFound();
            }
            ViewData["result"] = Result;
            return View();
        }
        private bool PatientExists(int id)
        {
            return _context.Patients.Any(e => e.Id == id);
        }
    }
}
